import * as type from '../types';

const initialState = {
    favourite: [],
    loading: false,
    error: null,
  }
  
  export default function favourite(state = initialState, action) {
    switch (action.type) {
      case type.GET_FAVOURITE_REQUESTED:
        return {
          ...state,
          loading: true,
        }
      case type.GET_FAVOURITE_SUCCESS:
        return {
          ...state,
          loading: false,
          favourite: action.payload
        }
      case type.GET_FAVOURITE_FAILED:
        return {
          ...state,
          loading: false,
          error: action.message,
        }
      default:
        return state
    }
  }