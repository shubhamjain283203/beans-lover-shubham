import { combineReducers } from 'redux';
import search from './user';
import favourite from './favourite'

const rootReducer = combineReducers({
    search: search,
    favourite: favourite
});

export default rootReducer;