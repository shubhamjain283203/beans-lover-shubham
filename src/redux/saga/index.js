import { all } from 'redux-saga/effects'
import searchSaga from './userSaga'
import favouriteSaga from './favouriteSaga'
export default function* rootSaga() {
  yield all([
    searchSaga(),
    favouriteSaga()
  ])
}